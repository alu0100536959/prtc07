require 'spec_helper'
require 'prct07'
#Crear it para saber de que clase son las pruebas
#introducir todas las preguntas hechas practica anterior
#describe Prct07 do

describe Prct07::SimpleExpec do
            before :each do
		
			@lista_doble = Prct07::Lista_doble.new
             
         	@preg1=Prct07::SimpleExpec.new(
				      :text => "salida de :  class Objeto \n     end",
				      :right => "Una instancia de la clase Class",
				      :distractor => ['una constante', 'un objeto', 'ninguna de las anteriores'])
				
	        @nodo1=Prct07::Nodo.new(@preg1, nil,nil)    
            end	

end
    describe Prct07::VerdFals do
        
 				@preg2=Prct07::VerdFals.new(
 				      :text => "Es apropiado que una clase tablero herede de la clase juego? ",
 				      :verd => "Falso", 
 				      :fals => ['Cierto'])
 				
 				@nodo2=Prct07::Nodo.new(@preg2, nil,nil)
    end
 	           
 	           
    describe Prct07::Lista_doble do
         before :each do
		
			@lista_doble = Prct07::Lista_doble.new
             
         	@preg1=Prct07::SimpleExpec.new(
				      :text => "salida de :  class Objeto \n     end",
				      :right => "Una instancia de la clase Class",
				      :distractor => ['una constante', 'un objeto', 'ninguna de las anteriores'])
				
	        @nodo1=Prct07::Nodo.new(@preg1, nil,nil)   
	        @preg2=Prct07::VerdFals.new(
 				      :text => "Es apropiado que una clase tablero herede de la clase juego? ",
 				      :verd => "Falso", 
 				      :fals => ['Cierto'])
 				
 			@nodo2=Prct07::Nodo.new(@preg2, nil,nil)
            end	
  	 context "Creacion de lista;" do
  	     it"Empujamos al principio" do
  	         
  	     @lista_doble.push_principio(@nodo1)
  	     @lista_doble.push_principio(@nodo2)
  	     end
  	     it"Eliminamos al final" do
  	     @lista_doble.pop_final
  	     end
  	     it "existe un metodo insertar_mitad_delante" do
 		 expect(@lista_doble).to respond_to :insertar_mitad_delante
         end
 	     
     end
    end
